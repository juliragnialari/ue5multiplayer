// Copyright Epic Games, Inc. All Rights Reserved.

#include "OverSweatedGameMode.h"
#include "OverSweatedCharacter.h"
#include "UObject/ConstructorHelpers.h"

AOverSweatedGameMode::AOverSweatedGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
