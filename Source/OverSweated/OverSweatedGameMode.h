// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OverSweatedGameMode.generated.h"

UCLASS(minimalapi)
class AOverSweatedGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOverSweatedGameMode();
};



