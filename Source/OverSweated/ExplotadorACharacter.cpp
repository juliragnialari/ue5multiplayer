// Fill out your copyright notice in the Description page of Project Settings.


#include "ExplotadorACharacter.h"

#include "Net/UnrealNetwork.h"

// Sets default values
AExplotadorACharacter::AExplotadorACharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bAlwaysRelevant = true;
	bNetLoadOnClient = false;
}

// Called when the game starts or when spawned
void AExplotadorACharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void AExplotadorACharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AExplotadorACharacter, isAttacking);
}

// Called every frame
void AExplotadorACharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AExplotadorACharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

