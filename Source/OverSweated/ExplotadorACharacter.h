// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ExplotadorACharacter.generated.h"

class AOverSweatedCharacter;

UCLASS()
class OVERSWEATED_API AExplotadorACharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AExplotadorACharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Replicated);
	bool isAttacking;
	UPROPERTY(EditAnywhere,BlueprintReadWrite);
	AOverSweatedCharacter* Target;
	UPROPERTY(EditAnywhere,BlueprintReadWrite);
	bool isPursuing;

	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
